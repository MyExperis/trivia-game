import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
const url = "https://opentdb.com/api.php";
const headers = { Accept: "application/json"};

export default new Vuex.Store({
    
    state: {
        allQuestions: [],
        difficulty: "easy",
        score: 0,
        answers: [],
        category: 0,
    },

    mutations: {
        setAllQuestions(state, payload) {
            state.allQuestions = payload;
        },
        setDifficulty(state, payload) {
            state.difficulty = payload;
        },
        increaseScore(state, payload) {
            state.score += payload;
        },
        resetScore(state) {
            state.score = 0;
        },
        addAnswer(state, payload) {
            state.answers.push(payload);
        },
        resetAnswers(state) {
            state.answers = [];
        },
        setCategory(state, payload) {
            state.category = payload;
        }

    },

    actions: {
        async setAllQuestions(state, payload) {
            try {
                let questions;
                if (state.getters.getDifficulty == "any") {
                    questions = await fetch(`${url}?amount=${payload.nr}&category=${payload.cat}`, {headers});
                } else {
                    questions = await fetch(`${url}?amount=${payload.nr}&category=${payload.cat}&difficulty=${state.getters.getDifficulty}`, {headers});
                }
                const json = await questions.json();
                console.log(json)
                if (json.response_code == 0) {

                    state.commit("setAllQuestions", json.results);

                } else if (json.response_code == 1) {

                    state.commit("setAllQuestions", [json.response_code])
                }
            } catch (error) {
                console.log(error);
            }
        },
        setDifficulty(state, payload) {
            state.commit("setDifficulty", payload);
        },
        increaseScore(state, payload) {
            state.commit("increaseScore", payload);
        },
        resetScore(state) {
            state.commit("resetScore");
        },
        addAnswer(state, payload) {
            state.commit("addAnswer", payload);
        },
        resetAnswers(state) {
            state.commit("resetAnswers");
        },
        setCategory(state, payload) {
            state.commit("setCategory", payload);
        }
    },

    getters: {
        getAllQuestions: state => state.allQuestions,
        getDifficulty: state => state.difficulty,
        getScore: state => state.score,
        getAnswers: state => state.answers,
        getCategory: state => state.category,
    }
});
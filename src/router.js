import VueRouter from "vue-router";
import StartScreen from "./views/StartScreen.vue";
import QuestionScreen from "./views/QuestionScreen";
import ResultScreen from "./views/ResultScreen"

const routes = [{
    path: "/", 
    name: "StartScreen", 
    component: StartScreen
}, 
{
    path: "/questions", 
    name: "QuestionScreen", 
    component: QuestionScreen
}, 
{
    path: "/result", 
    name: "ResultScreen", 
    component: ResultScreen
}];

const router = new VueRouter ({ 
    mode: "history",
    routes });

export default router;